﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SqlSugar;
using TanMiniToolSet.Common;

namespace SSO.Web
{
    public class DB
    {
        private DB()
        {
 
        }
        public static string ConnectionString = ConfigHelper.GetConfigString("ConnectionString");
        private static SqlSugarClient _db;
        public static SqlSugarClient GetInstance()
        {
            if (_db == null)
            {
                _db = new SqlSugarClient(ConnectionString);
            }
            return _db;
        }
    }
}
